<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MahasiswaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [MahasiswaController::class, 'index']);
Route::get('/mahasiswa/export', [MahasiswaController::class, 'export'])->name('mahasiswa.export');
Route::post('/mahasiswa/import', [MahasiswaController::class, 'import'])->name('mahasiswa.import');