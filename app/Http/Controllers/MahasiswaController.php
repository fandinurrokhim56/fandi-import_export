<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MahasiswaModel;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

class MahasiswaController extends Controller
{
    public function index()
    {
        $mahasiswa = MahasiswaModel::all();
        return view('index', [
            'data' => $mahasiswa
        ]);
    }

    public function export()
    {
        $data = DB::table('mahasiswa')->get();


        $spreadsheet = new Spreadsheet();

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Nama Mahasiswa');
        $sheet->setCellValue('B1', 'Fakultas');
        $sheet->setCellValue('C1', 'Prodi');
        $sheet->setCellValue('D1', 'No Telpon');
        $sheet->setCellValue('E1', 'Jenis Kelamin');
        $sheet->setCellValue('F1', 'Alamat');
        $sheet->setCellValue('G1', 'Tanggal Lahir');

        // STYLE DARI CELLS
        // Berisi autosize, border, fill untuk header
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);

        $headerStyleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ],
            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => 'b0ffb0',
                ],
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
        ];
        $sheet->getStyle('A1:G1')->applyFromArray($headerStyleArray);

        // isi dari tabel dimasukan kesini dan style nya juga
        // style berisi all border dan warna fill untuk data ganjil
        $row = 2;
        foreach ($data as $index => $mahasiswa) {
            $sheet->setCellValue('A' . $row, $mahasiswa->nama_mahasiswa);
            $sheet->setCellValue('B' . $row, $mahasiswa->fakultas);
            $sheet->setCellValue('C' . $row, $mahasiswa->prodi);
            $sheet->setCellValue('D' . $row, $mahasiswa->no_telpon);
            $sheet->setCellValue('E' . $row, $mahasiswa->jenis_kelamin);
            $sheet->setCellValue('F' . $row, $mahasiswa->alamat);
            $sheet->setCellValue('G' . $row, $mahasiswa->tanggal_lahir);

            $sheet
                ->getStyle('A' . $row . ':G' . $row)
                ->getAlignment()
                ->setHorizontal(Alignment::HORIZONTAL_LEFT);

            if ($index % 2 != 0) {
                $sheet
                    ->getStyle('A' . $row . ':G' . $row)
                    ->getFill()
                    ->setFillType(Fill::FILL_SOLID);
                $sheet
                    ->getStyle('A' . $row . ':G' . $row)
                    ->getFill()
                    ->getStartColor()
                    ->setARGB('DEE2E6');
            }

            $sheet->getStyle('A' . $row . ':G' . $row)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

            $row++;
        }

        // export dan output menjadi file excel
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Data Mahasiswa.xlsx"');
        header('Cache-Control: max-age=0');

        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
    }

    public function import(Request $request)
    {
        try {
            $request->validate([
                'file' => 'required|mimes:xlsx,xls',
            ]);

            $file = $request->file('file');
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);
            $worksheet = $spreadsheet->getActiveSheet();
            $rows = $worksheet->toArray();

            foreach ($rows as $key => $row) {
                if ($key === 0) {
                    continue;
                }

                DB::table('mahasiswa')->insert([
                    'nama_mahasiswa' => $row[0],
                    'fakultas' => $row[1],
                    'prodi' => $row[2],
                    'no_telpon' => $row[3],
                    'jenis_kelamin' => $row[4],
                    'alamat' => $row[5],
                    'tanggal_lahir' => $row[6],
                ]);
            }

            return redirect('/')->with('success', 'Data Mahasiswa berhasil diimport.');
        } catch (\Exception $e) {
            return redirect('/')->with('error', 'Terjadi kesalahan saat mengimpor data, pastikan kolom sesuai');
        }
    }

}